import * as React from 'react'

// MaterialUI
import Card from 'material-ui/lib/card/card'
import CardActions from 'material-ui/lib/card/card-actions'
import CardTitle from 'material-ui/lib/card/card-title'
import CardText from 'material-ui/lib/card/card-text'
import FlatButton from 'material-ui/lib/flat-button'

export interface IProps {
    title: string
    name: string
    actionName: string
    onAction: any
}

export default class BaseForm extends React.Component<IProps, any> {

    render() {
        return (
            <Card style={{
                margin: 20,
                padding: 20
            }}>
                <CardTitle title={this.props.title}></CardTitle>
                <CardText>
                    <p>Hello {this.props.name}</p>
                    <p>Please, {this.props.actionName} your request</p>
                </CardText>
                <CardActions>
                </CardActions>
                <FlatButton
                    onTouchTap={this.props.onAction}
                    label={this.props.actionName}>
                </FlatButton>
            </Card>
        )
    }
}
