import * as React from 'react'

import BaseForm from './BaseForm'

export interface IProps {
    name: string
    onCreate: any
}

export default class Create extends React.Component<IProps, any> {

    render() {
        return (
            <BaseForm
                name={this.props.name}
                title="Create request"
                actionName="create"
                onAction={this.props.onCreate}
            />
        )
    }
}
