/*
 * Action Types
 */

import { SET_TASK, SET_TASK_OUTCOME } from '../constants/Task.ts'

/*
 * Action Creators
 */

export function setTask(task) {
  return { type: SET_TASK, task }
}

export function updateOutcome(outcome: string) {
    return { type: SET_TASK_OUTCOME, outcome }
}
