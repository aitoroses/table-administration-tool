import {getAPIFactory, IAPIClient, IAPIClientProxy, IDescriptor} from '../services/AdminTableAPI'
import { Map, List, fromJS } from 'immutable'

export const SET_ALL = '@@SET_ALL'
export const ADD_TO_EDITING = '@@ADD_TO_EDITING'

export interface IAPIActions<T> extends IAPIClient<T> {
    addToEditingQueue<T>(descriptor: IDescriptor, entity: T): AddToQueue<T>
}

export interface IAPIActionsMap {
    [x: string]: IAPIActions<any>
}

export async function createActions<T>(): Promise<IAPIActionsMap> {
    let api = await getAPIFactory()
    return Object.getOwnPropertyNames(api).reduce((acc: any, k) => {
        acc[k] = actionCreatorFactory(api[k])
        return acc
    }, {})
}


interface SetAllAction<T> {
    type: string
    descriptor: IDescriptor
    data: List<T>
}

interface AddToQueue<T> {
    type: string
    descriptor: IDescriptor
    entity: List<T>
}

type Actions<T> = SetAllAction<T> | AddToQueue<T>


/**
 * Creates a set of actions that will interact
 * with the AdminTableAPI and its state
 */
export function actionCreatorFactory<T>(client: IAPIClient<T>) {

    // Action Creators
    function setData(descriptor: IDescriptor, data: List<T>): SetAllAction<T> {
        return { type: SET_ALL, descriptor, data }
    }

    // Asynchronous Actions

    function addToEditing(entity: T) {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            dispatch({ type: ADD_TO_EDITING, descriptor: desc, entity})
        }
    }

    function persistChange() {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            // We get from the state que current queue
            let state: IState = getState().admin
            let editingMap = state.getIn([desc.tableName, 'editing'])
        }
    }

    function getAll() {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            let data = await client.getAll()
            dispatch( setData(desc, fromJS(data)) )
            return data
        }
    }

    function get(query) {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            let data = await client.get(query)
            dispatch( setData(desc, fromJS(data)) )
            return data
        }
    }

    function update(obj: Map<string, any>) {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            let data = await client.update(obj.toJS())
        }
    }
    function remove(obj: Map<string, any>) {
        return async (dispatch, getState) => {
            let desc = await client.descriptor()
            let data = await client.remove(obj.toString())
            return data
        }
    }

    return {
        addToEditing,
        getAll,
        get,
        update,
        remove,
    }
}

// This is the recordType for IState, but not supported by typescript
// interface ITableState {
//     // Elements being edited
//     editing: Map<T, A>
//
//     // Current data
//     rows: List<Map<string, any>>
// }

type IState = Map<string, any>

export function reducer<T>(state: IState = Map<string, any>(), action: Actions<T>) {
        switch(action.type) {

            case SET_ALL: {
                let a = action as SetAllAction<T>
                let tableName = a.descriptor.tableName
                return state.setIn([tableName, 'rows'], a.data)
            }

            // Add entity to editing map
            case ADD_TO_EDITING: {
                let a = action as AddToQueue<T>
                let tableName = a.descriptor.tableName
                let queue = state.updateIn([tableName, 'editing'], (map) => {
                    if (map) {
                        return map.set(a.entity, a.entity)
                    } else {
                        return Map().set(a.entity, a.entity)
                    }
                })
            }

            default:
                return state
        }
    }
