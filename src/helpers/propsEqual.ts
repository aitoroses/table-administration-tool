const shallowEqual = require('react-pure-render/shallowEqual')

export default function propsEqual<T>(keys: string[]) {
    return (a: T) => (b: T) => {
        function pick(obj) {
            keys.reduce(function(acc, k) {
                acc[k] = obj[k]
                return acc
            }, {})
        }

        return shallowEqual(pick(a), pick(b))
    }
}
