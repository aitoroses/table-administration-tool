/*
 * Task Constants
 */

export const SET_TASK = 'SET_TASK'
export const SET_TASK_OUTCOME = 'SET_TASK_OUTCOME'
