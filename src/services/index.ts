import {Injector} from 'angular2-di'

import {TaskService} from './TaskService'

export const injector = Injector.resolveAndCreate([
  TaskService
])

export const taskService: TaskService = injector.get(TaskService)
