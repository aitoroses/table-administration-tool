import ServiceWrapper from './ServiceWrapper'

export interface IDescriptor {
    tableName: string
    primaryKey: string
    path: string
    allowedActions: string[]
    fields: IDescriptorField[]
}

interface IDescriptorField {
    label: string
    key: string
    config: IDescriptorFieldConfig
    mock: IDescriptorFieldMock
}

interface IDescriptorFieldConfig {
    type: string
    editable: string
    validations: string
    tooltip: string
    example: string
    format: string
}

interface IDescriptorFieldMock {
    generator: string
    template: string
}


// Obtain an http client instance
const http = new ServiceWrapper

export interface IAPIClient<T> {
        getAll: () => Promise<T[]>
        get: (query: {
            filter?: string
            filterBy?: string[],
            order?: string
            orderBy?: string
            skip?: number
            limit?: number
        }) => Promise<T[]>
        update: (x: T) => Promise<boolean>
        remove: (x: string) => Promise<T[]>
        descriptor: () => Promise<IDescriptor>
}

/**
 * IAPIClient is the generated object from the descriptors
 */
export interface IAPIClientProxy {
    [x: string]: IAPIClient<any>
}

/**
 * This object will store table cache
 * @type {Object}
 */
const cacheMap = {
    TABLES: null
}

/**
 * Fetch a descriptor by name
 * @param  {String} name Name of the descriptor
 * @return {Promise<Object>} Resulting descriptor
 */
async function fetchDescriptor(name): Promise<IDescriptor> {
    // Return a promise with the descriptor
    if (!cacheMap[name]) {
        let {data} = await http.get(`configuration/${name}`)
        cacheMap[name] = data.table
        return cacheMap[name]
    } else {
        return cacheMap[name]
    }
}

/**
 * Fetch table names
 */
async function fetchTables() {
    if (!cacheMap["TABLES"]) {
        let tables: string[] = (await http.get('configuration/admin-tables')).data
        cacheMap["TABLES"] = tables

        return tables
    } else {
        return cacheMap["TABLES"]
    }
}

/**
 * IAPIClient Factory function
 * Will fetch the tables and return a cached proxy object
 * @return {Promise<IAPIClient>}
 */
export async function getAPIFactory<T>(): Promise<IAPIClientProxy> {
    let tables = await fetchTables()

    let proxy: any = {}

    tables.forEach(t => {

        // Define properties to proxy object
        // Each table returns a Client
        Object.defineProperty(proxy, t, {
            get: () => {
                // Get the descriptor
                let descriptor = fetchDescriptor(t)
                return generateAPI(descriptor)
            }
        })
    })

    return proxy
}

/**
 * This is the table API object
 */
function generateAPI<T>(descriptorPromise: Promise<IDescriptor>): IAPIClient<T> {

    // This function entures that the descriptor has been already fetched
    function wrap(fn) {
        return async function(...args) {
            let d = await descriptorPromise
            return fn.apply(null, [d, ...args])
        }
    }

    const getAll = async function(desc: IDescriptor) {
        let {data} = await http.get(`${desc.path.slice(1)}/all`)
        return data
    }

    const get = async function(desc: IDescriptor, query) {
        let {data} = await http.get(desc.path.slice(1), query)
        return data
    }

    const update = async function(desc: IDescriptor, obj) {
        let {data} = await http.post(`${desc.path.slice(1)}/update`, obj)
        return data
    }

    const remove = async function(desc: IDescriptor, obj) {
        // TODO: Generate the PK of the object with handlebars
        let pk = desc.primaryKey
        let {data} = await http.post(`${desc.path.slice(1)}/remove`, pk)
        return data
    }

    const descriptor = async function(desc: IDescriptor, obj) {
        return desc
    }

    return {
        getAll: wrap(getAll),
        get: wrap(get),
        update: wrap(update),
        remove: wrap(remove),
        descriptor: wrap(descriptor)
    }
}
