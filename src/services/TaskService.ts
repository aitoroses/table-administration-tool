import ServiceWrapper from './ServiceWrapper'
import {HumanWorkflow, AuthenticateInput, TOKEN_UPDATED, TokenAction, QueryFilter} from 'bss-hw-api'
import {ITask} from '../interfaces/task'

// For dispatching tokens
import {store} from '../store/configureStore'
import {setToken} from '../actions/identity'

export class TaskService extends ServiceWrapper {

  public hw: HumanWorkflow

  constructor() {
    super()
    this.hw = new HumanWorkflow(this.url)

    // Connect to our store
    this.hw.subscribe((a: TokenAction) => {
      if (a.type == TOKEN_UPDATED) {
        if (a.token) {
          store.dispatch(setToken(a.token))
        }
      }
    })
  }

  authenticate(payload: AuthenticateInput) {
    return this.hw.authenticate(payload)
  }

  queryTasks(query: QueryFilter): Promise<ITask[]> {
    return this.hw.queryTasks(this.apiKey, query) as any
  }

  queryTasksCount(query: QueryFilter): Promise<ITask[]> {
    return this.hw.queryTasksCount(this.apiKey, query) as any
  }

  queryTasksPaginated(query: QueryFilter, skip: number, limit: number): Promise<ITask[]> {
    return this.hw.queryTasksByPage(this.apiKey, query, skip, limit) as any
  }

  queryTaskDetails(taskId: string): Promise<ITask> {
    return this.hw.getTaskDetailsById(this.apiKey, taskId) as any
  }

  updateTask(task: ITask) {
    return this.hw.updateTask(this.apiKey, task)
  }
}

export default new TaskService()
