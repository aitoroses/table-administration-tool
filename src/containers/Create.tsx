
import * as React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { createSelector } from 'reselect'
import * as axios from 'axios'

const lsbus = require('lsbus')

import Auth from '../decorators/Auth'
import { getLoggedUser } from '../reducers/identity'
import { authToken } from 'config'

import Create from '../components/Create'

// State selector
const mapStateToProps = createSelector(
    // This way we are accessing user's 5-2-1
    state => getLoggedUser(state),
    user => ({ user })
)

export interface IProps {
    user: string
    location
    dispatch
}

import { createActions } from '../helpers/tableModelRedux'

@Auth.container
@connect(mapStateToProps)
class CreateContainer extends React.Component<IProps, any> {

    handleCreate = async (e) => {

        // Navigate Back in the workspace
        let response = await axios.post('/api/test/instantiate', {}, {
            headers: {
                // Send authorization
                authorization: `Bearer ${localStorage.getItem(authToken)}`
            }
        })
        lsbus.send('workspace', {
            type: 'WORKSPACE_GO_BACK'
        })
    }


    async componentDidMount() {
            let api = await createActions()
            let r1 = await this.props.dispatch(api["NP5_COUNTRY"].getAll())
            let r2 = await this.props.dispatch(api["NP5_COUNTRY"].get({filterBy: ['countryName'], filter: 've'}))
            console.log(r1, r2)
        }

    render() {
        return (
            <Create
                name={this.props.user}
                onCreate={this.handleCreate}
                />
        )
    }
}

export default CreateContainer
