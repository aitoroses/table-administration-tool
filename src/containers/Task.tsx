
import * as React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { createSelector } from 'reselect'

import { ITask } from 'bss-hw-api'

const lsbus = require('lsbus')

import { taskService } from '../services'
import * as taskActions from '../actions/task'
import Auth from '../decorators/Auth'
import { getLoggedUser } from '../reducers/identity'
import { getTask } from '../reducers/task'
import Approve from '../components/Approve'
import EditDraft from '../components/EditDraft'

// State selector
const mapStateToProps = createSelector(
    // This way we are accessing user's 5-2-1
    getLoggedUser,
    getTask,
    (user, task) => ({ user, task })
)

export interface IProps {
    task: ITask
    user: string
    location
    dispatch
}

@Auth.container
@connect(mapStateToProps)
class Task extends React.Component<IProps, any> {

    public actions: typeof taskActions

    constructor(props) {
        super()
        this.actions = bindActionCreators(taskActions, props.dispatch)
    }

    async componentDidMount() {
        const taskId = this.props.location.query.bpmWorklistTaskId
        const task = await taskService.queryTaskDetails(taskId)
        this.actions.setTask(task)
    }

    handleApprove = async (e) => {
        this.actions.updateOutcome('APPROVE')
        await taskService.updateTask(this.props.task)

        // This will make the workspace
        // close the form and refresh
        lsbus.send('workspace', {
            type: 'WORKSPACE_CLOSE_HW'
        })
    }

    handleSubmit = async (e) => {
        this.actions.updateOutcome('SUBMIT')
        await taskService.updateTask(this.props.task)
        lsbus.send('workspace', {
            type: 'WORKSPACE_CLOSE_HW'
        })
    }

    render() {
        if (this.props.task) {

            switch (this.props.task.systemAttributes.taskDefinitionId) {

                case "default/eAppKernel!1.0/EditDraftHumanTask":
                    return (
                        <EditDraft
                            name={this.props.user}
                            onSubmit={this.handleSubmit}
                            />
                    )

                case "default/eAppKernel!1.0/ApproveHumanTask":
                    return (
                        <Approve
                            name={this.props.user}
                            onApprove={this.handleApprove}
                            />
                    )

                default:
                    return <div>Not a valid <strong>TaskDefinitionID</strong></div>
            }
        }

        return (
            <div>Loading...</div>
        )
    }
}

export default Task
