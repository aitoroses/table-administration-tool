import { decodeToken, JWT } from 'bss-hw-api'
import { SET_TOKEN, REMOVE_TOKEN } from '../constants/Identity'

import * as Joi from 'joi'

function decodeSafe(token) {
    try {
        return decodeToken(token)
    } catch(e) {
        return null
    }
}

import { authToken } from 'config'

let token = localStorage.getItem(authToken);
let tokenInfo = decodeSafe(token)

// TokenInfo Schema
const TokenInfo = Joi.object().keys({
  workflowContext: Joi.string().required(),
  locale: Joi.string().required(),
  profile: Joi.object().keys({
    displayName: Joi.string().required()
  }).unknown().required(),
  sub: Joi.string().required(),
  exp: Joi.number().required(),
  iat: Joi.number(),
  apps: Joi.object().unknown()
}).unknown()

export default function auth(state = { token, tokenInfo }, action?) {

  switch (action.type) {

    case SET_TOKEN:

      let tkInfo = decodeSafe(action.token)

      // Run validation
      Joi.validate(tkInfo, TokenInfo, (err, v) => err ? console.error(err): null)

      localStorage.setItem(authToken, action.token)
      return Object.assign({}, state, { token: action.token, tokenInfo: tkInfo})

    case REMOVE_TOKEN:
      localStorage.removeItem(authToken)
      return Object.assign({}, state, { token: null, tokenInfo: null })

    default:
      return state
  }
}

export function getJWT(state): JWT {
  try {
    return decodeToken(state.identity.token)
  } catch(e) {
    return null
  }
}

export function getLoggedUser(state): string {
  try {
    return getJWT(state).sub
  } catch(e) {
    return 'anonymous'
  }
}

export function getLoggedUserDisplayName(state): string {
  try {
    return getJWT(state).profile.displayName
  } catch(e) {
    return 'No user'
  }
}

export function getUserLanguage(state): string {
  try {
    return getJWT(state).locale
  } catch(e) {
    return null
  }
}

export function getWorkflowContext(state): string {
  try {
    return getJWT(state).workflowContext
  } catch(e) {
    return null
  }
}

export function getProfile(state) {
  try {
    return getJWT(state).profile
  } catch(e) {
    return null
  }
}

export function getApps(state) {
  try {
    return getJWT(state).apps
  } catch(e) {
    return null
  }
}
