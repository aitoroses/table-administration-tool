/**************************************************************************
 * Root reducer                                                           *
 *                                                                        *
 * We will pass to our store just one reducer to handle all the state     *
 * by combining all the reducers                                          *
 *                                                                        *
 * routerStateReducer will provide to the store the routers state so that *
 * we are able to extract it from the store                               *
 **************************************************************************/

import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

// Import reducers
import task from './task'
import identity from './identity'
import { reducer } from '../helpers/tableModelRedux'

const rootReducer = combineReducers({
    identity,
    task,
    admin: reducer,
	routing: routerReducer
})

export default rootReducer
