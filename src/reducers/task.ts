import { SET_TASK, SET_TASK_OUTCOME } from '../constants/Task.ts'
import { ITask } from 'bss-hw-api'

const initialState = null

export default function task(task: ITask = initialState, action) {

    switch (action.type) {

        case SET_TASK:
            return action.task

        case SET_TASK_OUTCOME:
            if (task) {
                (task as any).outcome = action.outcome
            }

            return task

        default:
            return task
    }
}

export function getTask(state) {
    return state.task
}
