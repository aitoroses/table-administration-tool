import * as React from 'react';
const getMuiTheme = require('material-ui/lib/styles/getMuiTheme').default

export default function MuiThemeProvider(muiTheme?) {
    return function(DecoratedComponent): typeof DecoratedComponent {

        const displayName =
            DecoratedComponent.displayName ||
            DecoratedComponent.name ||
            'Component'

        class MuiThemeProvider extends React.Component<any, any> {

            static displayName = `MuiThemeProvider(${displayName})`

            static childContextTypes = {
              muiTheme: React.PropTypes.object.isRequired,
            };

            getChildContext() {
              return {
                muiTheme: getMuiTheme(muiTheme),
              };
            }

            render() {
              return <DecoratedComponent {...this.props} />;
            }
        }

        return MuiThemeProvider;
    }
}
