//////////////////
// Dependencies //
//////////////////

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Router, Route } from 'react-router'
import { Provider } from 'react-redux'

///////////////////////
// Components import //
///////////////////////

import MuiThemeProvider from './decorators/MuiThemeProvider'

// -- Import components --
import Task from './containers/Task'
import Create from './containers/Create'

///////////
// Store //
///////////

import {storePromise} from './store/configureStore'

/*****************************************************************
 * Root component                                                *
 * Is the top level component, it wraps the application entirely *
 * for rerendering in case of changes in the Redux Store         *
 *                                                               *
 * Here we are defining the application routes.                  *
 *                                                               *
 * The MainHandler is the component that acts as layout for the  *
 * others, passed as children to it.                             *
 *****************************************************************/

@MuiThemeProvider()
class Root extends React.Component<any, any> {

    state = {
        store: null,
        DevTools: null,
        history: null
    }

    async componentDidMount() {
        this.setState(await storePromise)
    }

    render() {
        const {store, DevTools} = this.state

        if (!store) return <noscript />

        return (
            <Provider store={store}>
                <div>
                    <Router history={ this.state.history }>
                        {/* Routes */}
                        <Route path='/form' component={ Task } />
                        <Route path='/create' component={ Create } />
                    </Router>
                    { DevTools ? <DevTools store={store} /> : null }
                </div>
            </Provider>
        )
    }
}

///////////////////////////
// Application Bootstrap //
///////////////////////////

/***************************************************************************
 * The application render target is the root node in the index.html folder *
 *                                                                         *
 * In case that we are in development mode with DEBUG=true the redux state *
 * monitor will appear also.                                               *
 ***************************************************************************/
document.addEventListener('DOMContentLoaded', function() {

    // TODO: React v15 will not need this
    const injectTapEventPlugin = require('react-tap-event-plugin')
    injectTapEventPlugin()

    ReactDOM.render(
        <Root />,
        document.getElementById('root')
    )
})
