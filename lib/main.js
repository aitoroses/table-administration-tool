// Polyfill ES6
require('reflect-metadata')

// Global configuration
require('./config')

// Application entries
require('../assets/style/app.css')
require('../src')
