# Project structure

Inside the repository we will find the following folders:

* assets: Holds application fonts, images and style
  * Style is preprocessed with *PostCSS*
* docs: Application documentation
* lib: Application main files
  * config.js: Project global configuration variables
  * globals.js: Library global shims
  * main.js: Webpack target file
* src: Application sources
* static: Static content files
* templates: Plop template files
* test: Contains the mock server, fixtures and unit tests
* tools: Here we put the needed scripts and binaries
* webpack: This are the webpack config files
* workspace: Workspace and login distributions
* .babelrc: Babel configuration file
* .editorconfig: IDE configuration
  * Ensure to have installed the corresponding Atom plugin
* .gitignore: Git Ignored files
* index.html: Application entry file
* karma.conf.js: Karma Test Runner configuration file
* package.son: NPM manifest file
* plopfile.js: Plop template and scaffolder configuration file
* README.md: Main doc file
* tsconfig.json: Typescript configuration file
* typings.json: **typings** is the tool that installs typescript definition files for required libraries
