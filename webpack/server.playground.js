import webpack from 'webpack'
import express from 'express'
import compression from 'compression'
import httpProxy from 'http-proxy'

import WebpackDevServer from 'webpack-dev-server'

import config from './webpack.config.playground'

//////////////////////////////
// Configure WebPack server //
//////////////////////////////

let webpackDevServer = new WebpackDevServer(webpack(config), config.devServer)

// Start listening
webpackDevServer.listen(5000)
