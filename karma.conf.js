require('babel-register')

var webpack = require('webpack')
var path = require('path')

var webpackConfig = require('./webpack/webpack.config.base').default

webpackConfig.plugins = [
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify("process.env.NODE_ENV"),
        __DEV__: JSON.stringify(process.env.DEBUG),
        API_URL: JSON.stringify(process.env.API_URL),
    })
]

module.exports = function(config) {
    config.set({

        browserNoActivityTimeout: 30000,

        browsers: [process.env.CONTINUOUS_INTEGRATION ? 'Firefox' : 'Chrome'],

        singleRun: process.env.CONTINUOUS_INTEGRATION === 'true',

        frameworks: ['mocha', 'sinon-chai'],

        files: [
            'webpack/tests.webpack.js'
        ],

        preprocessors: {
            'webpack/tests.webpack.js': ['webpack', 'sourcemap']
        },

        reporters: ['mocha'],

        webpack: webpackConfig,

        webpackServer: {
            noInfo: true
        }

    })
}
