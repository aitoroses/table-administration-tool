import toml from 'toml'
import fs from 'fs'
import path from 'path'

const resolveDir = (dir) =>  path.resolve(path.join(__dirname, dir))
const tablesDir = resolveDir('../fixtures/admin')

export function dirTables() {
    let dirs = fs.readdirSync(tablesDir)
    return dirs
}

export function readTable(table) {
    let t = fs.readFileSync(path.join(tablesDir, table), 'utf-8')
    return toml.parse(t)
}

export function convertToMockModel(t) {


    function NamedFunction(name, args, body, scope, values) {
        if (typeof args == "string")
            values = scope, scope = body, body = args, args = [];
        if (!Array.isArray(scope) || !Array.isArray(values)) {
            if (typeof scope == "object") {
                var keys = Object.keys(scope);
                values = keys.map(function(p) { return scope[p]; });
                scope = keys;
            } else {
                values = [];
                scope = [];
            }
        }
        return Function(scope, "function "+name+"("+args.join(", ")+") {\n"+body+"\n}\nreturn "+name+";").apply(null, values);
    };

    // Edit this function to generate
    // the mock model for a specific field
    function getMock(field) {
        return {
            function() {
                if (field.mock && field.mock.generator) {
                    let fn = NamedFunction("Generator", [], field.mock.generator, this)
                    return fn.call(this)
                } else if (field.mock && field.mock.template) {
                    return this.faker.fake(field.mock.template)
                } else {
                    return this.faker.fake('{{lorem.word}}')
                }
            }
        }
    }

    let fields = t.table.fields
    return fields.reduce(function(acc, f) {
        acc[f.key] = getMock(f)
        return acc
    }, {})
}

export function getTableDescriptors() {
    return dirTables()
        .map(readTable)
}

export function getTableNames() {
    return getTableDescriptors()
        .map(x => x.table.tableName)
}

export function getTableMocksGenerators() {
     return getTableDescriptors()
        .reduce((acc, t) => {
            acc[t.table.tableName] = convertToMockModel(t)
            return acc
        }, {})
}
