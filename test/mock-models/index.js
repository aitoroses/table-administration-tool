var models = require('./models')


var mocks = models.reduce(function(acc, k) {
	acc[k] = require('./' + k)
	return acc
}, {})

// Add admin tables models
let adminMocks = require('./admin-table-model')
	.getTableMocksGenerators()
	
Object.keys(adminMocks).forEach((k) => {
	mocks[k] = adminMocks[k]
})

module.exports = mocks
