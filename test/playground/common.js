// Polyfill ES6
require('reflect-metadata')

// Global configuration
require('../../lib/config')

// Application entries
require('../../assets/style/app.css')
