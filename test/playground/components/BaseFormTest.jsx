import * as React from 'react'

import BaseForm from 'components/BaseForm'

class MyComponentTest extends React.Component {
  render() {
    return (
      <BaseForm {...this.props} />
    )
  }
}

module.exports = MyComponentTest
