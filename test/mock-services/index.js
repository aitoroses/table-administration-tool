module.exports = {
  hw: require('./human-workflow'),
  sso: require('./sso'),
  configuration: require('./configuration'),
  'test-services': require('./test-services'),
  'admin-services': require('./admin-services')
}
