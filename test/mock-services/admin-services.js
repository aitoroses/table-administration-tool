
import { getTableDescriptors, getTableNames } from '../mock-models/admin-table-model'
import { db } from '../mock-db'

const tables = getTableDescriptors()
const tableNames = getTableNames()

var template = '{{countryId}}-{{countryName}}'
var value = '1234-ES'


/**
 * Apply a handlebars template to a string to obtain the context
 */
function applyMask(template, value) {

    function getMatches(template) {
        var reg = /{{[a-zA-Z]+}}/g
        var matches = []
        var match
        while(match = reg.exec(template)) {
            matches.push(match)
        }
        return matches
    }

    function getRegexMask(template) {
        var matches = getMatches(template)
        matches.forEach(m => {
            template = template.replace(m[0], '([a-zA-Z0-9_-]+)')
        })
        return template
    }

    var matches = getMatches(template)
    var mask = getRegexMask(template)

    var result = {}

    matches.forEach((m, i) => {
        debugger
        result[m[0].replace(/{}/g, '')] = value.replace(new RegExp(mask), '$' + (i+1))
    })

    return result
}

/**
 * restifyDescriptor generates a set of CRUD operations for a Nocker server
 */
function restifyDescriptor(desc) {

    function filter(items, query) {
        if (!query.filter || !query.filterBy) return items
        var result = items
        var searchKeys = [].concat(query.filterBy)

        if (searchKeys.length > 0) {
          result = result.filter(function(item) {
            var anyMatch = searchKeys
              .map(function(k) {
                return (new RegExp(query.filter, 'i')).test(item[k])
              })
              .filter(function(c) {
                return c == true
              })

            return anyMatch.length > 0
          })
        }

        return result
      }

    const getAll = {
        method: 'GET',
        path: `${desc.table.path}/all`,
        reply(params, query, body) {
            db[desc.table.tableName].find({}).exec((err, res) => {
                this.res.json(res)
            })
        }
    }

    const get = {
        method: 'GET',
        path: `${desc.table.path}`, //?skip, limit,  order,   orderBy,   filter,   filterBy
        reply(params, query, body) {
            db[desc.table.tableName].find({})
                .skip(query.skip)
                .limit(query.limit)
                .sort({[query.orderBy]: query.order == "DSC" ? -1 : 1})
                .exec((err, res) => {
                    console.log(query)
                    this.res.json(filter(res, query))
                })
        }
    }


    const create = {
        method: 'POST',
        path: `${desc.table.path}/create`,
        reply(params, query, body) {
            db[desc.table.tableName].insert(body, (err, res) => {
                this.res.status(201)
                this.res.json(res)
            })
        }
    }

    const update = {
        method: 'POST',
        path: `${desc.table.path}/update`,
        reply(params, query, body) {
            db[desc.table.tableName].update({_id: body._id}, {$set: body}, (err, res) => {
                this.res.status(200)
                this.res.json(res)
            })
        }
    }

    const remove = {
        method: 'GET',
        path: `${desc.table.path}/delete/:id`,
        reply(params, query, body) {
            let pk = desc.table.primaryKey
            let context = applyMask(pk, params.id)
            db[desc.table.tableName].remove(context, (err, res) => {
                this.res.status(200)
                this.res.end(res)
            })
        }
    }

    return [
        get,
        getAll,
        create,
        update,
        remove
    ]
}

// Reduce services for every table
const services = tables.map(restifyDescriptor)
    .reduce((acc, tableServices) => acc.concat(tableServices), [])

// Service for admin tables
const adminTables = {
    method: 'GET',
    path: `/configuration/admin-tables`,
    reply(params, query, body) {
        this.res.json(tableNames)
    }
}

// Table descriptor services
const tableDescriptorServices = tableNames.map( t => ({
    method: 'GET',
    path: `/configuration/${t}`,
    reply() {
        let descriptor = tables.filter(desc => desc.table.tableName == t)[0]
        this.res.json(descriptor)
    }
}))


const allServices = [adminTables, ...tableDescriptorServices, ...services]

module.exports = allServices
