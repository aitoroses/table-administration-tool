var path = require('path')
var NEDB = require('nedb')

var promises = []

function reducer(db, k) {
    db[k] = new NEDB({ filename: path.resolve('test/mock-db/' + k + '.db')})
    promises.push(new Promise(function(resolve) {
        db[k].loadDatabase(resolve)
    }))
    return db
}

// Include admin databases
let adminModels = require('../mock-models/admin-table-model')
    .dirTables().map(t => t.replace('.toml', ''))


var db = require('../mock-models/models').concat(adminModels).reduce(reducer, {})
var ready = Promise.all(promises)


module.exports = {
  db,
  ready
}
